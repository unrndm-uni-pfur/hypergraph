FROM python:3.9

ENV POETRY_VERSION=1.1.2 \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100

RUN chmod a+rwx /usr/local/bin
RUN chmod a+rwx /usr/local/lib/python*/site-packages

RUN bash -cl "pip install \"poetry==$POETRY_VERSION\""
RUN bash -cl "poetry config virtualenvs.create false"
