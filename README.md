Hypergraphs
===========
My implementation of Hypergraphs

[![Gitlab Pipeline Report][pipeline_report_url]][pipeline_url]
[![Gitlab COverage Report][coverage_url]]()

[![Gitpod ready-to-code][gitpod_badge]][gitpod_url]

[![Checked with mypy][mypy_badge]](http://mypy-lang.org/)
[![Code style: black][black_badge]](https://github.com/psf/black)
[![Imports: isort][isort_badge]](https://pycqa.github.io/isort/)

[![GitLab license][MIT_license_badge]][license_link]

has 3 main classes:
 * Vertex
 * Edge
 * Hypergraphs

has 1 example, can be run after installation with
```sh
python -m hypergraph
```

Tests
-----
This module has basic tests, can be run with
```sh
python -m pytest
```

Coverage
--------
To view coverage report, run 
```sh
python -m coverage run --source src -m pytest
python -m coverage report -m
```


<!-- links -->
[pipeline_report_url]: https://gitlab.com/unrndm/hypergraph/badges/master/pipeline.svg?style=flat-square
[pipeline_url]: https://gitlab.com/unrndm/hypergraph/-/pipelines

[coverage_url]: https://gitlab.com/unrndm/hypergraph/badges/master/coverage.svg?style=flat-square

[gitpod_badge]: https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod&style=flat-square
[gitpod_url]: https://gitpod.io/#https://gitlab.com/unrndm/hypergraph

[mypy_badge]: https://img.shields.io/badge/mypy-checked-2a6db2?style=flat-square
[black_badge]: https://img.shields.io/badge/code%20style-black-000000.svg?style=flat-square
[isort_badge]: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat-square&labelColor=ef8336

[MIT_license_badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[license_link]: https://gitlab.com/unrndm/hypergraph/blob/master/LICENSE
