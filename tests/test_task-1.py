# #!/usr/bin/env python3
# from __future__ import annotations

# from collections.abc import Callable

# from .hypergraph import Edge, Hypergraph, Vertex, WeightedEdge

# ppl = {char: Vertex(char) for char in ["person1", "person2", "person3"]}
# edu = {char: Vertex(char) for char in ["education1", "education2", "education3"]}
# pos = {char: Vertex(char) for char in ["position1", "position2", "position3"]}

# v = {}
# v.update(ppl)
# v.update(edu)
# v.update(pos)

# we: list[WeightedEdge[tuple[float, float]]] = [
#     WeightedEdge([v["person1"], v["education1"], v["position1"]], (1, 10)),
#     WeightedEdge([v["person1"], v["education2"], v["position1"]], (2, 11)),
#     WeightedEdge([v["person2"], v["education2"], v["position2"]], (3, 25)),
#     WeightedEdge([v["person1"], v["education2"], v["position2"]], (2, 1)),
#     WeightedEdge([v["person3"], v["education3"], v["position3"]], (1, 5)),
# ]
# h = Hypergraph(v.values(), we)
# res = h.task1(ppl.values(), edu.values(), pos.values())
# print(res)
