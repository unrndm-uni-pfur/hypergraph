from .hypergraph import Edge, Hypergraph, Vertex


def main():
    v = {"a": Vertex("a"), "b": Vertex("b"), "c": Vertex("c"), "d": Vertex("d")}
    e = [
        Edge([v["a"], v["b"]]),
    ]
    g = Hypergraph(list(v.values()), e)
    print("hypergraph main function")


if __name__ == "__main__":
    main()
