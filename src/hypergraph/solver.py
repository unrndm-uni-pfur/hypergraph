from __future__ import annotations

from functools import reduce
from itertools import cycle

from .hypergraph import Edge, Hypergraph


class Solver:
    @staticmethod
    def solve_task_1(
        hg: Hypergraph,
        applicants: Iterable[Vertex],
        education: Iterable[Vertex],
        positions: Iterable[Vertex],
    ) -> tuple[tuple[float, float], ...]:
        """решение для задачи `Двукритериальная задача кадрового менеджмента`

        Parameters
        ----------
        hg : Hypergraph
            гиперграфф, на котором надо искать решение
        applicants : Iterable[Vertex]
            итерабельный объект, соответствующий множеству претендентов
        education : Iterable[Vertex]
            итерабельный объект, соответствующий множеству должностей
        positions : Iterable[Vertex]
            итерабельный объект, соответствующий множеству видов обучения претендентов

        Returns
        -------
        tuple[tuple[float, float], tuple[float, float]]
        """
        # 3-дольном 3-однородном
        assert hg.is_weighted
        assert len(applicants) == len(education) == len(positions)

        combinations: List[List[Edge]] = []
        comb_weight: List[List[Any]] = []

        a = cycle(applicants)
        e = cycle(education)
        p = cycle(positions)

        for i_a in range(len(applicants)):
            for i_e in range(len(education)):
                for i_p in range(len(positions)):
                    edge_vertices: list[Vertex] = []
                    edge_vertices.append([next(a) for _ in range(len(applicants))])
                    edge_vertices.append([next(e) for _ in range(len(education))])
                    edge_vertices.append([next(p) for _ in range(len(positions))])

                    edges = [Edge(ev) for ev in edge_vertices]
                    combinations.append(edges)
                    next(p)
                next(e)
            next(a)

        for comb in combinations:
            comb_weight.append([])

            for comb_edge in comb:
                for edge in hg.edges:
                    if edge == comb_edge:
                        comb_weight[-1].append(edge.weight)

        f: tuple[float, ...] = tuple()
        f_comb: tuple[float, ...] = tuple()
        for idx, edge in enumerate(comb_weight):
            if len(edge) == 0:
                continue
            if f[0] is None and f[1] is None:
                f[0] = sum((w[0] for w in edge))
                f[1] = sum((w[1] for w in edge))

                f_comb = tuple((combinations[idx] for _ in range(len(f))))
            else:
                cur_f: tuple[float, ...] = tuple()

                cur_f[0] = sum((w[0] for w in edge))
                if cur_f[0] > f[0]:
                    f[0] = cur_f[0]
                    f_comb[0] = combinations[idx]

                cur_f[1] = sum((w[1] for w in edge))
                if cur_f[1] > f[1]:
                    f[1] = cur_f[1]
                    f_comb[1] = combinations[idx]

        return tuple((f[i], f_comb[i]) for i in range(len(f)))

    @staticmethod
    def solve_task_2(
        hg: Hypergraph,
        programs: Iterable[Vertex],
        measurers: Iterable[Vertex],
        satellites: Iterable[Vertex],
    ) -> tuple[tuple[float, float], tuple[float, float], tuple[float, float]]:
        """решение для задачи `Математическая модель задачи управления космическим командно-измерительным комплексом`

        Parameters
        ----------
        hg : Hypergraph
            гиперграфф, на котором надо искать решение
        programs : Iterable[Vertex]
            итерабельный объект, соответствующий множеству программ
        measurers : Iterable[Vertex]
            итерабельный объект, соответствующий множеству измерительных пунктов космического комплекса
        satellites : Iterable[Vertex]
            итерабельный объект, соответствующий множеству спутников

        Returns
        -------
        tuple[tuple[float, float], tuple[float, float], tuple[float, float]]
        """
        # 3-дольном 3-однородном
        assert hg.is_weighted
        assert len(programs) == len(measurers) == len(satellites)
        n = len(programs)

        p = cycle(programs)
        m = cycle(measurers)
        s = cycle(satellites)
        combinations: List[List[Edge]] = []

        for i_p in range(len(programs)):
            for i_m in range(len(measurers)):
                for i_s in range(len(satellites)):
                    edge_vertices: list[Vertex] = []
                    edge_vertices.append([next(p) for _ in range(len(programs))])
                    edge_vertices.append([next(m) for _ in range(len(measurers))])
                    edge_vertices.append([next(s) for _ in range(len(satellites))])

                    edges = [Edge(ev) for ev in edge_vertices]
                    combinations.append(edges)
                    next(s)
                next(m)
            next(p)

        comb_weight: List[List[Any]] = []

        for comb in combinations:
            comb_weight.append([])
            for comb_edge in comb:
                for edge in hg.edges:
                    if edge == comb_edge:
                        comb_weight[-1].append(edge.weight)

        f: tuple[float, ...] = tuple()
        f_comb: tuple[float, ...] = tuple()
        for idx, edge in enumerate(comb_weight):
            if len(edge) == 0:
                continue
            if (f[0] is None) and (f[1] is None) and (f[2] is None):
                f[0] = sum((w[0] for w in edge))
                f[1] = sum((w[1] for w in edge))
                f[2] = sum((w[2] for w in edge))

                f_comb = tuple((combinations[idx] for _ in range(len(f))))
            else:
                cur_f: tuple[float, ...] = tuple()

                cir_f[0] = min((w[0] for w in edge))
                if cur_f[0] > f[0]:
                    f[0] = cur_f[0]
                    f1_comb = combinations[idx]

                cur_f[1] = sum((w[1] for w in edge))
                if cur_f[1] < f[1]:
                    f[1] = cur_f[1]
                    f2_comb = combinations[idx]

                cur_f[2] = reduce(
                    lambda x, y: x * y, (w[2] for w in edge), initializer=1
                )
                if cur_f[2] > f[2]:
                    f[2] = cur_f[2]
                    f2_comb = combinations[idx]

        return tuple((f[i], f_comb[i]) for i in range(len(f)))

    @staticmethod
    def solve_task_3(
        hg: Hypergraph,
        groups: Iterable[Vertex],
        instructors: Iterable[Vertex],
        methods: Iterable[Vertex],
    ) -> tuple[tuple[float, float], tuple[float, float], tuple[float, float]]:
        """решение для задачи `Математическая модель обучения сотрудников организации`

        Parameters
        ----------
        hg : Hypergraph
            гиперграфф, на котором надо искать решение
        groups : Iterable[Vertex]
            итерабельный объект, соответствующий множеству групп учебного центра
        instructors : Iterable[Vertex]
            итерабельный объект, соответствующий множеству инструкторов учебного центра
        methods : Iterable[Vertex]
            итерабельный объект, соответствующий множеству методов обучения

        Returns
        -------
        tuple[tuple[float, float], tuple[float, float], tuple[float, float]]
        """
        # 3-дольном 3-однородном
        assert hg.is_weighted
        assert len(groups) == len(instructors) == len(methods)

        g = cycle(groups)
        i = cycle(instructors)
        m = cycle(methods)
        combinations: List[List[Edge]] = []

        for i_g in range(len(groups)):
            for i_i in range(len(instructors)):
                for i_m in range(len(methods)):
                    edge_vertices: list[Vertex] = []

                    edge_vertices.append([next(g) for _ in range(len(groups))])
                    edge_vertices.append([next(i) for _ in range(len(instructors))])
                    edge_vertices.append([next(m) for _ in range(len(methods))])

                    edges = [Edge(ev) for ev in edge_vertices]

                    combinations.append(edges)

                    next(m)
                next(i)
            next(g)

        comb_weight: List[List[Any]] = []

        for comb in combinations:
            comb_weight.append([])

            for comb_edge in comb:
                for edge in hg.edges:
                    if edge == comb_edge:
                        comb_weight[-1].append(edge.weight)

        f: tuple[float, ...] = tuple()
        f_comb: tuple[float, ...] = tuple()
        for idx, edge in enumerate(comb_weight):
            if len(edge) == 0:
                continue
            if (f[0] is None) and (f[1] is None) and (f[2] is None):
                f[0] = sum((w[0] for w in edge))
                f[1] = sum((w[1] for w in edge))
                f[2] = sum((w[2] for w in edge))

                f_comb = tuple((combinations[idx] for _ in range(len(f))))
            else:
                cur_f: tuple[float, ...] = tuple()

                cir_f[0] = sum((w[0] for w in edge))
                if cur_f[0] > f[0]:
                    f[0] = cur_f[0]
                    f1_comb = combinations[idx]

                cur_f[1] = sum((w[1] for w in edge))
                if cur_f[1] > f[1]:
                    f[1] = cur_f[1]
                    f2_comb = combinations[idx]

                cur_f[2] = sum((w[2] for w in edge))
                if cur_f[2] > f[2]:
                    f[2] = cur_f[2]
                    f2_comb = combinations[idx]

        return tuple((f[i], f_comb[i]) for i in range(len(f)))

    @staticmethod
    def solve_task_4(
        hg: Hypergraph,
        teachers: Iterable[Vertex],
        education: Iterable[Vertex],
        classes: Iterable[Vertex],
    ) -> tuple[tuple[float, float], tuple[float, float], tuple[float, float]]:
        """решение для задачи `Математическая модель назначения учителей в классы с учетом технологий обучения.`

        Parameters
        ----------
        hg : Hypergraph
            гиперграфф, на котором надо искать решение
        teachers : Iterable[Vertex]
            итерабельный объект, соответствующий множеству учителей
        education : Iterable[Vertex]
            итерабельный объект, соответствующий множеству современных педагогических технологий обучения
        classes : Iterable[Vertex]
            итерабельный объект, соответствующий множеству классов данной параллели

        Returns
        -------
        tuple[tuple[float, float], tuple[float, float], tuple[float, float]]
        """
        # 3-дольном 3-однородном
        assert hg.is_weighted
        assert len(teachers) == len(education) == len(classes)

        t = cycle(teachers)
        e = cycle(education)
        c = cycle(classes)
        combinations: List[List[Edge]] = []

        for i_t in range(len(teachers)):
            for i_e in range(len(education)):
                for i_c in range(len(classes)):
                    edge_vertices: list[Vertex] = []

                    edge_vertices.append([next(t) for _ in range(len(teachers))])
                    edge_vertices.append([next(e) for _ in range(len(education))])
                    edge_vertices.append([next(c) for _ in range(len(classes))])

                    edges = [Edge(ev) for ev in edge_vertices]

                    combinations.append(edges)

                    next(c)
                next(e)
            next(t)

        comb_weight: List[List[Any]] = []

        for comb in combinations:
            comb_weight.append([])

            for comb_edge in comb:
                for edge in hg.edges:
                    if edge == comb_edge:
                        comb_weight[-1].append(edge.weight)

        f: tuple[float, ...] = tuple()
        f_comb: tuple[float, ...] = tuple()
        for idx, edge in enumerate(comb_weight):
            if len(edge) == 0:
                continue
            if (f[0] is None) and (f[1] is None) and (f[2] is None):
                f[0] = min((w[0] for w in edge))
                f[1] = sum((w[1] for w in edge))
                f[2] = sum((w[2] for w in edge))

                f_comb = tuple((combinations[idx] for _ in range(len(f))))
            else:
                cur_f: tuple[float, ...] = tuple()

                cir_f[0] = min((w[0] for w in edge))
                if cur_f[0] > f[0]:
                    f[0] = cur_f[0]
                    f1_comb = combinations[idx]

                cur_f[1] = sum((w[1] for w in edge))
                if cur_f[1] > f[1]:
                    f[1] = cur_f[1]
                    f2_comb = combinations[idx]

                cur_f[2] = sum((w[2] for w in edge))
                if cur_f[2] > f[2]:
                    f[2] = cur_f[2]
                    f2_comb = combinations[idx]

        return tuple((f[i], f_comb[i]) for i in range(len(f)))
